function mijnFunctie(inputfunctie) {
  const resultaat = 12;
  setTimeout(function() {
    inputfunctie(resultaat);
  }, 3000);
}

mijnFunctie(function(a) {
  console.log("a = ", a);
});
