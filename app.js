const http = require("http");
const database = require("./src/database");

// const hostname = "localhost";
const port = process.env.PORT || 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader("Content-Type", "application/json");

  const movie = {
    title: "Hier de titel",
    waarde: 12,
    lijst: [{ test: "Hier een tekst" }]
  };
  database.movies.push(movie);

  res.end(JSON.stringify(database));
});

server.listen(port, function() {
  // console.log(`Server running at http://${hostname}:${port}/`);
  console.log(`Server running at ${port}`);
});
